package cl.cencosud.configuration;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class DriverFactory {
	
	public static WebDriver getDriver() {
//	    boolean flagExt = Boolean.parseBoolean(System.getProperty("ext", "false"));
//	    String size = flagExt ? "start-maximized" : "--window-size=1200,1200";
	    System.setProperty("webdriver.chrome.driver",
	        "src/test/resources/drivers/windows/chromedriver.exe");
	    ChromeOptions options = new ChromeOptions();
	    options.setExperimentalOption("useAutomationExtension", false);
	    options.setExperimentalOption("excludeSwitches",
	        Collections.singletonList("enable-automation"));
	    options.addArguments("disable-infobars");
//	    options.addArguments("incognito");
	    WebDriver driver = new ChromeDriver(options);
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	    driver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
	    return driver;
	  }

	
	//driver de selenoid
	public static WebDriver remotewebdriver() throws MalformedURLException {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName("chrome");
		capabilities.setVersion("75.0");
		capabilities.setCapability("enableVNC", true);
		capabilities.setCapability("enableVideo", false);

		RemoteWebDriver driver = new RemoteWebDriver(
		    URI.create("http://10.95.2.125:4444/wd/hub").toURL(), 
		    capabilities
		    
		   );
		
		return driver;
	}

}
