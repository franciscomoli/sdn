package cl.cencosud.dataproviders;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.testng.annotations.DataProvider;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import cl.cencosud.models.BusquedaGoogle;

public class DataProviders {
	
	@DataProvider
	public static Object[][] datos() throws JsonSyntaxException, JsonIOException, FileNotFoundException{
	
	Gson gson = new Gson();
	
	BusquedaGoogle busquedaGoogle = gson.fromJson(new FileReader("src/test/resources/data/datos.json"), BusquedaGoogle.class);
	return new Object[][] {{busquedaGoogle}};
}

}
