package cl.cencosud.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultgooglePage {

	private WebDriver driver;

	public ResultgooglePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "resultStats")
	private WebElement resultado;
	
	
	public String  validarresultado() {
		return resultado.getText();
	}
}
