package cl.cencosud.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GooglePage {
	
	private WebDriver driver;
	
	public GooglePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="q")
	private WebElement txtgoogle;
	
	@FindBy(name="btnK")
	private WebElement botonbuscar;
	
	public ResultgooglePage buscar(String busqueda) {
		
		txtgoogle.sendKeys(busqueda);
		botonbuscar.click();
		return new ResultgooglePage(driver);
	}
	

}
